---
title: Installation
---

# Installation
## Information

::: tip
In this page, you'll find informtion about how to installing Quasar project from scratch. This maybe helpful sometimes. So, this page is included. If you want to clone project from git and then work just go to the previous page. Below, we listed the installing modes of Quasar.
:::

Firsly, we need to know what is **Quasar** and **Quasar CLI** before creating a project. Quasar is upgraded and boosted material-design components and styles set. And using Quasar we can easily create **SPA**, **PWA**, **BEX**, **Mobile** and **PC applications**. You'll find out more in the **Project modes** section.

Secondly, Quasar has CLI mode which is integrated with your device CMD or Terminal app. Quasar CLI doesn't need any links to **CDNs** or **Static files**. But if you don't use CLI mode, you need to link Quasar static files(CSS, JS and etc) or their CDN links to your ***.html** files. You can easily create, build and manage the project with it. 

**USING CLI MODE IS RECOMMENDED.**

## Before creating

::: warning
To install Quasar make sure all the things listed below are exist on your device. And if you don't have any of that things, you can follow the urls after them to install. Installing x64 bit and last version is recommended. If there is any listed, specific version, they are required.
:::

| Required   | Version |                                           Installation link |
| :--------- | :-----: | ----------------------------------------------------------: |
| **NodeJS** | Latest  |                        [NodeJS.Org](https://nodejs.org/en/) |
| **NPM**    | Latest  |         [NpmJS.Com](https://www.npmjs.com/package/download) |
| **Yarn**   | Latest  | [YarnPkg.Com](https://classic.yarnpkg.com/en/docs/install/) |
| **VueJS**  |   2.x   |      [VueJS.Org](https://vuejs.org/guide/installation.html) |
| **Quasar** | Latest  |    [Quasar.Dev](https://quasar.dev/quasar-cli/installation) |

## Creating new project

Now we can open the directory(where the new project should be created) using CMD/Terminal to create a new project. To create new project write the following script.

```bash{2}
# When not worked. See global Quasar installation 
quasar create [project-name]
# Write project name instead of [project-name]
```

After pressing enter key the Quasar starter kit download proccess will begin. Then, it asks for project name. Write it with lowercase letters and hit enter.

**For instance:** gobazar

And now enter product name. Product name is the name which will be displayed to the user as app or website name. You can use capital letters but should be written with a letter at least.

**For instance:** GoBazar

Now you're asked to enter project description. Just enter what the project is about.

**For instance:** A Quasar based E-Commerce app to help people with cheap and fast product delivery.

So, after the desicription, you should enter the author name and then an E-mail address to show the project owner and his E-mail address. By default it shows your Git credentials. Press any key to write custom author info. When writing custom info write E-mail address between ``<>`` signs like below.

```bash
GoBazar Team <gobazar@gmail.com>
```

Now you should enter favorite CSS preprocessor.
Quasar prefers for the first(Sass with indented syntax) option on this way and **SASS is recommended** by Quasar Team. But you can use second recommended(Sass with SCSS syntax) option and **SCSS of SASS is good choice** and looks like CSS but with more features. And you left 2 options. One is **most common CSS preprocessor called Stylus**. But it is not recommended now. And the **second left option is "None" which is the other options available or just Pure CSS**. And GoBazar uses SCSS option for the GoBazar frontend project.

**For insctance selected:** *> Sass with SCSS*

Now Quasar asks for the strategy which components & directives importing included. And has 3 options. You can see each option description in the below table.

| Import option         | Description                                                                                                                                                         |
| :-------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **Auto import**       | Import Quasar components & directives which you used automatically. Slightly higher compile time. Next to minimum bundle size. But this is most convenient one.     |
| **Manual import**     | Manually specify what to import and what to use - fastest compile time. Mminimum bundle and more preferable otpion. But requires time to specify things one by one. |
| **Import everything** | Imports everything from Quasar. And everything is ready to use. But get more compiling time and large bundle size. Just the most unworthy option.                   |

**For insctance selected:** *> Auto import*

And now it asks important options, which is required to add or not to include packages. These packages are recommended by Quasar for general projects. And they are useful for any project. **But we don't need all of them.** Let's see what they are and what each option does.

| Package          | What it does and why used?                                                                                                                                                                                                                                                                 |
| :--------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **ESLint**       | ESLint is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code, with the goal of making code more consistent and avoiding bugs.                                                                                                                            |
| **Vuex**         | Vuex is a state management pattern + library for Vue. js applications. It serves as a centralized store for all the components in an application, with rules ensuring that the state can only be mutated in a predictable fashion.                                                         |
| **TypeScript**   | TypeScript is a superset of JavaScript which primarily provides optional static typing, classes and interfaces. One of the big benefits is to enable IDEs to provide a richer environment for spotting common errors as you type the code.                                                 |
| **Axios**        | Axios is a Promise-based HTTP client for JavaScript which can be used in your front-end application and in your Node. js backend. By using Axios it's easy to send asynchronous HTTP request to REST endpoints and perform CRUD operations.                                                |
| **Vue-i18n**     | Vue I18n is internationalization plugin for Vue.js. You can introduce internationalization into your app with simple API. In addition to simple translation, support localization such as pluralization, number, datetime ... etc. You can manage locale messages on single file component |
| **IE11 Support** | If you are building a website, you might want to support IE 11+. This support is not added by default to your website. Quasar CLI provides it on demand only.                                                                                                                              |

**For instance selected:** Vuex, Axios, Vue-i18n, IE11 Support

Now you can see it is asking Cordova/Capacitor ID after selecting needed packages. By default, it is ***org.cordova.quasar.app***. And you can specify your domain, a dot, website name, a dot, what the app name is and press enter to go to the next option.

**For instance:** com.gobzar.app

And now the last option: Should we run install script for you and which package manager should we use?
Always as the first one is the recommended, now the first one Yarn is recommended now, too. We use Yarn for GoBazar frontend project. And has 2 more options NPM and others. But Yarn is recommended for this project.

**For instance:** Yarn

Now, it begins to install dependencies and apply the info we entered. And one by one it installs all packages and modules. If there is any warning, it is just informaton about deprecated, upgraded or removed modules. Just ignore warnings.

It is ready!!! Now our project is created.
To run it on the **dev-server** first go to the directory with ``cd [project-name]``. Now we can run ``quasar dev``.
Now you can see it automatically opened the project in the default browser with localhost:8080 by default. And for more Quasar scripts find the sidebar section menu of **Quasar commands**.

If you want to change dev server host, open **quasar.conf.js** file and find the section which is written devServer. In that section uncomment line *port: 8080* and edit it. If it is not uncommented just edit.