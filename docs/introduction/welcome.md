---
title: Welcome
---
# Welcome
## What is GoBazar?

![alt text](/welcome.jpg "Welcome to GoBazar documentation")

GoBazar (pronounced /ɡəʊ · bəˈzɑː(r)/, like Go Bazaar) is an ecommerce website and application for PCs(Window, Mac and Linux), Mobile phones(IOS, Android). Unlike other ecommerce websites, you can view where is your ordered product is coming live. And we send messages from the receiving or sending points. You can easily buy anything you want and with delivery. Delivery is for Uzbekistan, Great Britain and USA for now but we're planning to run this project and our services in many countries too.

This is FrontEnd documentation for GoBazar project. You can easily find anything you want with the sidebar menu on the right side. And if you want to find things more faster, You can use search input which is on the beginning right part of the navigation bar.

GoBazar frontend is built with Quasar(Material design) and its components. There are some libraries used to make GoBazar more powerful. You can find them on the right side drawer, too. GoBazar uses Quasar framework of VueJs which is itself a framework of JavaScript. So GoBazar is mainly build with reactiveness.

If you’d like to learn more about GoBazar begin reading the next pages of the DOCS. Or if you need anything. Find needed information from this documentation.

## What's this docs and why used for?

 This is the GoBazar frontend project documentation and it is used to search and find anything used, included, written, coded in the project. So this documentation is much useful and has beautiful UI look. So this is nicer than any other documentations.
 
 This is used to learn, find and get some information about the project. This docs involves many tools like searching, routing, language adding features and back to top button, beautifully created navbar, the best mobile look, sidebar, auto selecting current link, shows last updted date, with many markdown extesnions, custom homepage, code blocks and fully markdown support. This docs is so small that will never become MB and is KB if no any pictures.

## What are included in this docs?

In the project documentation you find as much as information about using compoenents, plugins, libraries, code, style and much more. So and you find information about project installation and you can find some instructions to the best coding practises. And as we said above. You'll find anything about the project GoBazar. And more things are coming soon...