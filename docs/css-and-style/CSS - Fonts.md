---
title: Fonts
---

# Using fonts in project.
## We've custom fonts: ``Franchise`` and ``OpenSans``.

> The Franchise font doesn't have ``font-types``. But the ``OpenSans`` has. You can use default ``OpenSans`` and two other versions. There are **bold** and **semibold** types of ``OpenSans``.
We added ``@font-faces`` and set them as **root elements**. They are now global.

#### Here is the global variables of fonts.
```css
    --franchise: 'Franchise';
    --opensans: 'OpenSans';
    --os-semibold: 'OpenSans-SemiBold';
    --os-bold: 'OpenSans-Bold';
    --on-regular: 'Proxima-Regular';
```

#### Here is how to using global variables in CSS.
```css
    font-family: var(--franchise)';
    font-family: var(--opensans)';
    font-family: var(--opensans-semibold)';
    font-family: var(--opensans-bold)';
    font-family: var(--pn-regular)';
```

#### Here is how to using above in HTML as classes
```html
    <div class="text-h2 franchise">GoBazar</div>
    <div class="text-h3 opensans">GoBazar</div>
    <div class="text-h4 opensans-semibold">GoBazar<div>
    <div class="text-h5 opensans-bold">GoBazar</div>
    <div class="text-h5 proxima-regular">GoBazar</div>
```

> **Note**: *All the font files are stored in ``src/css/fonts`` folder and there is only unique ``fontkit.css`` file inside of the ``src/css`` folder. This font file is included in **css part** of the ``quasar.conf.js`` file.*