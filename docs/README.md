---
home: true
heroImage: /hero.png
heroText: GoBazar
tagline: Frontend Documentation
actionText: Get Started →
actionLink: /introduction/welcome
features:
- title: Simplicity First
  details: Minimal setup with markdown-centered project structure helps you focus on reading and to understand easily.Even add more new parts in mintues.
- title: Vue-Powered
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop your custom documentation with Vue.
- title: Performant
  details: You write MD and get pre-rendered static HTML. What you see is what will you get in the future. Run as SPA, PWA and SSR once the page's loaded.
footer: MIT Licensed | Copyright © 2019-present by GoBazar DevTeam
---