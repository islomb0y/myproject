---
title: Go Read more
---

# Go Read More

## What is GoReadMore?

Go-Readmore is a vue-based global component for th project. And it helps you to truncate texts and vice verca. When truncated it show read-more and when not truncated read-less words. You can specify any words to it by yourself.

## Feautures
- Add read more length
- Add read more link
- Custom read more string
- Custom read less string
    
## How to use it?

Without calling as a component! And just write``<go-readmore></go-readmore>`` tag and specifyattributes which are required to show read-moreand read-less.

## Usage

```html
<go-readmore more-str="read more" :text="msg" link="#" less-str="read less" :max-chars="50"></go-readmore>
```

## Examples
```html
<template>
  <div class="hello">
    <go-readmore more-str="read more" :text="msg" link="#" less-str="read less" :max-chars="50"></go-readmore>
    <go-readmore more-str="read more" less-str="read less" :text="msg2" link="#"></go-readmore>
  </div>
</template>

<script>
export default {
  data () {
    return {
      msg: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
      msg2: 'Lorem ipsum dolor sit amet'
    }
  }
}
</script>
```

**More examples coming soon...**


## API

| Option            | Type     | Default     | Description                                                                                    |
| ----------------- | -------- | ----------- | ---------------------------------------------------------------------------------------------- |
| `text` (required) | `String` | `none`      | `text` is used to limit the submitted string based on the `maxChars`.                          |
| `maxChars`        | `Number` | `100`       | It is used to limit the `text` value to generate a read more text.                             |
| `link`            | `String` | `#`         | You can pass a link incase you want to redirect the user when the `moreStr` has been triggerd. |
| `moreStr`         | `String` | `read more` | You can customize your read more string by passing a value to `moreStr`.                       |
| `lessStr`         | `String` | `none`      | You can customize your read less string by passing a value to `lessStr`.                       |

*Note:*
When you didn't pass something in the `lessStr`, after clicking the read more you wouldn't have a read less link.

## News

**Using as vue-directive feature and more coming soon...**