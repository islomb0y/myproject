---
title: Go route link
---

# Go route link

## What is GoRoute?

GoRoute is a custom router-link text button-like component for specific parts of GoBazar. For instance, this is used in cart and many other places.
This helps us to go some route and be displayed as text. But we can customize text color, size and others.

## Usage

```html
<go-route link="/cart" property="text-black" label="Hello">{{$t('viewBasket')}}</go-route>
```

::: tip Information
``<go-route></go-route>`` is tag for routing link. And the on side that doesn't look like ``<a></a>`` tag is it just goes with VueRouter routing and don't go with URL instead. So, clarifying: ``<go-route></go-route>`` used to go somewhere like ``/place`` and while ``<a></a>`` tag is used to go somewhere like ``https://place.domain``.
:::

- Has opportunity of using classes inside ``property`` prop attribute.
- You can display text with ``slot`` usage or with just prop attribute of **label** like ``label="some word"``.

- To pass functions in label like **ternary** or **i18n** functions you can just bind the label prop attribute like this:
   ``:label=""$t`` ``('translationWordKey')``
   or
   ```:label="1 !== 1 ? 'Hello' : 'Bye'"```.
- And what about ``using with slot(s)``. Usage is the same like using in VueJS:
   ``<go-route>{{$t`` ``('translationWordKey')}}</go-route>``
   or
   ```<go-route>{{1 !== 1 ? 'Hello' : 'Bye'}}</go-route>```.

-  Or even maybe with filters
   ```:label="wordFromData | truncate(5, '...')"```
   and by using slot
   ```<go-route>Lorem ipsum dolor sit amet | truncate(5, '...')</go-route>```. 
```bash
Output is :  Lorem...
# Text was: Lorem ipsum dolor sit amet.
# By truncating. We get 5 character and ...
```

## News
**More usages: COMING SOON...**