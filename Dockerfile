# build stage
FROM node:13.7-alpine as build
WORKDIR /app
COPY package.json /app/package.json
RUN yarn global add @quasar/cli
RUN apk --no-cache add pkgconfig autoconf automake libtool nasm build-base zlib-dev
COPY . /app
RUN yarn 
RUN quasar build

# production environment
FROM nginx:1.17
COPY --from=build /app/dist/spa /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]