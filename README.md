# GoBazar (gofront)

A delivery on billions of items with prime. Low prices across central Asia biggest selection of books, music, DVDs, electronics, computers, software, apparel ...

## Install the dependencies

``` bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

``` bash
quasar dev
```

### Build the app for production

``` bash
quasar build
```

### Start the app in mobile debug mode (hot-code reloading, error reporting, etc.)

``` bash
yarn cordova:dev
```

### Build the mobile app for production

``` bash
yarn cordova:build
```

## Install the Vue-awesome-swiper

> Whenever VueAwesomeSwiper is not found on your machine.

``` bash
yarn add vue-awesome-swiper --save
```

### Opening documentation

``` bash
yarn docs:dev
```

### Whenever Vuepress not found

``` bash
# Add Vuepress
yarn -D add vuepress
# Go into the docs folder
cd docs
# Make sure if all packages are installed
yarn
```

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).