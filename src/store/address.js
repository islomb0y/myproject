import { addressService, AuthenticationError } from '../services/address.service'


const state = {
    addresses: '',
    createdStatus: '',
    setDefaultStatus: '',
    editAddress: '',
    editedStatus: '',
    createdDetail: '',
    deletedData: '',
    sendedSMS: '',
    homePageProducts: '',
    newhomePageProducts: [],
    supportDetail: '',
    errorCode: '',
    errorMessage: '',

}

const getters = {
    addresses: (state) => {
        return state.addresses
    },
    createdStatus: (state) => {
        return state.createdStatus
    },
    setDefaultStatus: (state) => {
        return state.setDefaultStatus
    },
    getAddressDetail: (state) => {
        return state.editAddress
    },
    editedStatus: (state) => {
        return state.editedStatus
    },
    deletedData: (state) => {
        return state.deletedData
    },
    createdDetail: (state) => {
        return state.createdDetail
    },
    sendedSMS: (state) => {
        return state.sendedSMS
    },
    homePageProducts: (state) => {
        return state.homePageProducts
    },
    newhomePageProducts: (state) => {
        return state.newhomePageProducts
    },
    supportDetail: (state) => {
        return state.supportDetail
    },
    errorCode: (state) => {
        return state.errorCode
    },
    errorMessage: (state) => {
        return state.errorMessage
    },
}

const actions = {
    async getListAddresses({ commit }) {
        let addresses = await addressService.getListAddress();
        commit('listAddress', addresses)
    },
    async createAddress({ commit }, data) {
        try {
            let newAddress = await addressService.createAddress(data);
            commit('newAddress', newAddress)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async setDefault({ commit }, address_id) {
        try {
            let setDefault = await addressService.setDefault(address_id);
            commit('setDefault', setDefault)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async getAddressDetails({ commit }, address_id) {
        let addressDetail = await addressService.getEditAddress(address_id);
        commit('setDetail', addressDetail)
    },
    async updateAddress({ commit }, payload) {
        try {
            let update = await addressService.editAddress(payload.address_id, payload.data);
            commit('updateAddress', update)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async isDeleteAddress({ commit }, payload) {
        try {
            let deleted = await addressService.deleteAddress(payload);
            commit('deleteAddress', deleted)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async userCreatePaymentCard({ commit }, payload) {
        try {
            let request = await addressService.userCreatePaymentCard(payload);
            commit('createCard', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async sendSMSCodeVerfy({ commit }, payload) {
        try {
            let request = await addressService.sendSMSCodeVerfy(payload);
            commit('sendSMS', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async getHomePageProducts({ commit }) {
        try {
            let request = await addressService.getHomePageProducts();
            commit('homePageProducts', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async newHomePageProducts({ commit }) {
        try {
            
            let request = await addressService.newHomePageProducts();
            commit('newhomePageProducts', request)
        } catch (e) {
            commit('newhomePageProducts', undefined)
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },
    async sendSupport({ commit }, data) {
        try {
            commit('support', '')
            let support = await addressService.sendSupport(data);
            commit('support', support)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', { errorCode: e.errorCode, errorMessage: e.message })
            }
        }
    },

}

const mutations = {
    listAddress(state, addresses) {
        state.addresses = addresses
    },
    newAddress(state, status) {
        state.createdStatus = status
    },
    setDefault(state, status) {
        state.setDefaultStatus = status
    },
    setDetail(state, addressDetail) {
        state.editAddress = addressDetail
    },
    updateAddress(state, status) {
        state.editedStatus = status
    },
    deleteAddress(state, data) {
        state.addressDeleted = data
    },
    createCard(state, detail) {
        state.createdDetail = detail
    },
    sendSMS(state, detail) {
        state.sendedSMS = detail
    },
    homePageProducts(state, request) {
        state.homePageProducts = request
    },
    newhomePageProducts(state, request) {
        state.newhomePageProducts = request
    },
    support(state, payload) {
        state.supportDetail = payload
    },
    Error(state, { errorCode, errorMessage }) {
        state.errorCode = errorCode
        state.errorMessage = errorMessage
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}