import axios from 'axios'

const state = {
    accessToken: null,
    refreshToken: null,
    user: ''
  }
  
const mutations = {
  authUser (state, userData) {
    state.accessToken = userData.accessToken
    state.refreshToken = userData.refreshToken
  },
  storeUser (state, user) {
    state.user = user
  },
  clearAuthData(state) {
    state.accessToken = localStorage.getItem('access_token') || null;
    state.refreshToken = localStorage.getItem('refreshToken') || null;
    state.user = ''
  }
}
  
const actions = {
  setLogoutTimer({commit}, expirationTime) {
    setTimeout(() => {
      commit('clearAuthData')
    }, expirationTime * 1000)
  },
  signUp({dispatch}, authData) {
    axios.post('http://localhost:8000/api/auth/v1/create/', {
      user: {
        email: authData.email,
        password: authData.password
        }
      })
      .then(res => {
        const now = new Date()
        const expirationDateTime = new Date(now.getTime() + 3600 * 1000)
        localStorage.setItem('access_token', res.data.access)
        localStorage.setItem('expirationDateTime', expirationDateTime)
      })
      dispatch('setLogoutTimer', 36000)
      .catch(error => console.log(error))
  },
  login({commit, dispatch}, authData) {
    axios.post("http://localhost:8000/auth-token/create/", {
        email: authData.email,
        password: authData.password
      })
      .then(res => {
        const now = new Date()
        const expirationDateTime = new Date(now.getTime() + 3600 * 1000)
        localStorage.setItem('access_token', res.data.access)
        localStorage.setItem('expirationDateTime', expirationDateTime)

        commit('authUser', {
          accessToken: res.data.access,
          refreshToken: res.data.refresh
        })
      })
      dispatch('setLogoutTimer', 36000)
      .catch(error => console.log(error))
  },
  logout({commit}) {
    commit('clearAuthData')
    localStorage.removeItem('expirationDateTime')
    localStorage.removeItem('access_token')
    $router.replace("/login")
  },
  fetchUser({commit, state}) {
    axios.get("http://localhost:8000/api/auth/v1/me/", {
      headers: {Authorization: "Bearer "  + state.accessToken}
    })
    .then(res => {
      commit('storeUser', res.data.user)
    })
    .catch(error => console.log(error))
  }
}

const getters = {
  user (state) {
    return state.user
  },
  isAuthenticated(state) {
    return state.accessToken !== null
  }
}

export default {
  // namespaced: true,
  state,
  mutations,
  actions,
  getters
}