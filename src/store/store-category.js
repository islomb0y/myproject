const state = {
    rootCategoryList: [],
    subCategoryList: [],
    productsCount: '',
    pathName: [],
    activeCatID: 0,
    activeCatName: "",
    loader: true,
    productListInCategory: [],
    trendingProducts:'',

}

const mutations = {
    updateLoader(state) {
        state.loader = true
    },
    setRootCategoryToState(state, categoryResponse) {
        state.rootCategoryList = categoryResponse.data.Cat
        state.pathName.splice(0, state.pathName.length)
        state.loader = false
    },
    setSubCategoryList(state, payload){
        state.subCategoryList = payload
    },
    setProductsCount(state, count) {
        state.productsCount = count
    },
    setSubCategoryToState(state, catObj) {
        for (let i = 0; i < state.pathName.length; i++) {
            if (catObj.pathObj["catID"] === state.pathName[i].catID) {
                let result = state.pathName[i].catID
                let element = state.pathName.splice(i, state.pathName.length - i)
            }
        }
        state.pathName.push(catObj.pathObj)
        state.activeCatID = catObj.pathObj["catID"]
        state.activeCatName = catObj.pathObj["catName"]
        state.subCategoryList = catObj.catResp.data.results
        if (catObj.catResp.data.results.length === 0) {
            state.productListInCategory = catObj.productList == undefined ? ['no product'] : catObj.productList
        }
        state.loader = false
    },

    setCatLoaderToState(state) {
        state.subCategoryList = []
        state.productsCount = ''
        state.loader = true
    },

    fetchProductList(state, payload) {
        state.productListInCategory = payload
        state.loader = false
    },
    setTrendingProducts(state, payload) {
        state.trendingProducts = payload
    },
}

import CategoryService from "../services/category/category.service";


const actions = {

    async getRootCategory({ commit }) {
        try {
            commit('setCatLoaderToState')
            let categoryResponse = await CategoryService.rootCategory()
            commit('setRootCategoryToState', categoryResponse)
        } catch (e) {
            console.log(e)
        }

    },

    async updateCategoryList({ commit }, payload) {
        try {
            commit('setCatLoaderToState')
            let catObj = {}
            let categoryResponse = await CategoryService.categoryList(payload.cid)
            catObj = {
                "catResp": categoryResponse,
                "pathObj": payload.cid
            }
            if (categoryResponse.data.count === 0) {
                let productList = await CategoryService.fetcProductList(payload)
                commit('setProductsCount', productList.data.total_products.count)
                catObj.productList = productList.data.products
            }
            commit('setSubCategoryToState', catObj)
        } catch (e) {
            console.log(e)
        }
    },
    async productList({ commit }, payload) {
        try {
            let response = await CategoryService.fetcProductList(20, payload['pagenum'], payload['catID'])
            commit('fetchProductList', response.data.products)
        } catch (e) {
            throw e
        }

    },
    updateLoader({ commit }) {
        commit('setCatLoaderToState')
    },
    setSubCategoryList({commit}){
        commit('setSubCategoryList', [])
        commit('fetchProductList', [])
    },
    async getTrendingProducts({commit}){
        try {
            let request = await CategoryService.trendingProducts()
            commit('setTrendingProducts', request)
        } catch(e)  {
            console.log(e)
        }
    },

    async getTrendingProductsWithCatId({commit}, catid){
        try {
            let request = await CategoryService.trendingProductsWithVatId(catid)
            commit('setTrendingProducts', request)
        } catch(e)  {
            console.log(e)
        }
    }
}

const getters = {
  trendingProducts: (state) => {
    return state.trendingProducts
},
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
}

