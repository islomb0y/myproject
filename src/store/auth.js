import { UserService, AuthenticationError } from '../services/user.service'
import { TokenService } from '../services/storage.service'

const state = {
    authenticating: false,
    accessToken: TokenService.getToken(),
    authenticationErrorCode: 0,
    authenticationError: '',
    refreshTokenPromise: null, // Holds the promise of the refresh token
    user: '',
    signUpError: '',
    signUpErrorCode: 0,
    signUpStatusOk: 0
}

const getters = {
    user: (state) => {
       return state.user
    },
    loggedIn: (state) => {
        return state.accessToken ? true : false
    },

    signUpStatusOk:(state) => {
        return state.signUpStatusOk
    },

    authenticationErrorCode: (state) => {
        return state.authenticationErrorCode
    },

    authenticationError: (state) => {
        return state.authenticationError
    },

    authenticating: (state) => {
        return state.authenticating
    },

    signUpErrorCode: (state) => {
        return state.signUpErrorCode
    },

    signUpError: (state) => {
        return state.signUpError
    },


}

const actions = {
    async login({ commit }, { email, password }) {
        commit('loginRequest');
        try {
            const token = await UserService.login(email, password);
            commit('loginSuccess', token)
                // Redirect the user to the page he first tried to visit or to the home view
            // this.$router.push(this.$router.history.current.query.redirect || '/');
         window.location.reload();
            return true
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('loginError', { errorCode: e.errorCode, errorMessage: e.message })
            }
            return false
        }
    },

    logout({ commit }) {
        UserService.logout()
        commit('logoutSuccess')
        this.$router.push('/login')
    },
    refreshToken({ commit, state }) {
        // If this is the first time the refreshToken has been called, make a request
        // otherwise return the same promise to the caller
        if (!state.refreshTokenPromise) {
            const p = UserService.refreshToken()
            commit('refreshTokenPromise', p)

            // Wait for the UserService.refreshToken() to resolve. On success set the token and clear promise
            // Clear the promise on error as well.
            p.then(
                response => {
                    commit('refreshTokenPromise', null)
                    commit('loginSuccess', response)
                },
                error => {
                    commit('refreshTokenPromise', null)
                }
            )
        }

        return state.refreshTokenPromise
    },
    async fetchUser({ commit }) {
        let user = await UserService.fetchUser();
        commit('storeUser', user)
    },
    async signUp({ commit }, payload) {
        commit('signUpRequest')
        try {
            let request = await UserService.signUp(payload);
            if(request.data.user.email){
                commit('signUpOk',200)
            }
            if(request.data.user.error){
                commit('signUpError', { errorCode: 200, errorMessage: request.data.user.error})
            }
            return true
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('signUpError', { errorCode: e.errorCode, errorMessage: e.message[0] })
            }
            return false
        }
    }
}

const mutations = {
    loginRequest(state) {
        state.authenticating = true;
        state.authenticationError = ''
        state.authenticationErrorCode = 0
    },

    loginSuccess(state, accessToken) {
        state.accessToken = accessToken
        state.authenticating = false;
    },

    loginError(state, { errorCode, errorMessage }) {
        state.authenticating = false
        state.authenticationErrorCode = errorCode
        state.authenticationError = errorMessage
    },

    signUpRequest(state) {
        state.signUpError = ''
        state.signUpErrorCode = 0
        state.signUpStatusOk = 0
    },

    signUpError(state, { errorCode, errorMessage }) {
        state.signUpErrorCode = errorCode
        state.signUpError = errorMessage
    },

    signUpOk(state, payload){
        state.signUpStatusOk = payload
    },

    logoutSuccess(state) {
        state.accessToken = ''
    },
    refreshTokenPromise(state, promise) {
        state.refreshTokenPromise = promise
    },
    storeUser(state, userData) {
        state.user = userData
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
