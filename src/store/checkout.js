import { checkoutService, AuthenticationError } from '../services/checkout.services'

const state =  {
    userActiveCart: '',
    removedStatus: '',
    increasedStatus: '',
    reducedStatus: '',
    movedStatus: '',
    orderDetail: '',
    checkoutDetail: '',
    paymentDetail: '',
    deliveryDetail: '',
    addToCartDetail: '',
    addToWishlistDetail: '',
    errorCode: '',
    errorMessage: '',

}

const getters = {
    addToCartDetail: (state) => {
        return state.addToCartDetail
    },
    addToWishlistDetail: (state) => {
        return state.addToWishlistDetail
    },
    userActiveCart: (state) => {
        return state.userActiveCart
    },
    removedStatus: (state) => {
        return state.removedStatus
    },
    increasedStatus: (state) => {
        return state.increasedStatus
    },
    reducedStatus: (state) => {
        return state.reducedStatus
    },
    movedStatus: (state) => {
        return state.movedStatus
    },
    orderDetail: (state) => {
        return state.orderDetail
    },
    checkoutDetail: (state) => {
        return state.checkoutDetail
    },
    paymentDetail: (state) => {
        return state.paymentDetail
    },
    deliveryDetail: (state) => {
        return state.deliveryDetail
    },
    errorCode: (state) => {
        return state.errorCode
    },
    errorMessage: (state) => {
        return state.errorMessage
    },
}

const actions = {
    async addProductToCart({ commit }, payload){
        try {
            let request = await checkoutService.addProductToCart(payload);
            commit('addToCart', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                localStorage.removeItem('user');
                commit('addToCart', undefined)
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async addProductToWishlist({ commit }, payload){
        try {
            let request = await checkoutService.addProductToWishlist(payload);
            commit('addToWishlist', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('addToWishlist', e.errorCode)
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getUserActiveCart({ commit }){
        try {
            let request = await checkoutService.getUserActiveCart();
            commit('userActiveCart', request)
            if (request.restored_message){
                localStorage.removeItem('user');
            }
        } catch (e) {
            commit('userActiveCart', undefined)
            localStorage.removeItem('user');
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async removeProductFromCart({ commit }, product_id){
        try {
            commit('removedStatus', 0)
            let request = await checkoutService.removeProductFromCart(product_id);
            commit('removedStatus', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('removedStatus', e.errorCode)
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async increaseQuantity({ commit }, payload){
        try {
            let request = await checkoutService.increaseQuantity(payload);
            commit('increasedStatus', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('increasedStatus', e.errorCode)
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async reduceQuantity({ commit }, payload){
        try {
            let request = await checkoutService.reduceQuantity(payload);
            commit('reducedStatus', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('reducedStatus', e.errorCode)
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async cartProductMoveToWishlist({ commit }, payload){
        try {
            let request = await checkoutService.cartProductMoveToWishlist(payload);
            commit('movedStatus', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async userCreateOrder({ commit }, address_id){
        try {
            let request = await checkoutService.userCreateOrder(address_id);
            commit('orderCreate', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getCheckoutDetail({ commit }){
        try {
            let request = await checkoutService.getCheckoutDetail();
            commit('checkoutDetail', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async makePayments({ commit }, payload){
        try {
            let request = await checkoutService.makePayments(payload);
            commit('paymentSuccess', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async setDeliveryMethod({ commit }, payload){
        try {
            let request = await checkoutService.setDeliveryMethod(payload);
            commit('setDelivery', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
}
const mutations = {
    addToCart(state, request) {
        state.addToCartDetail = request
    },
    addToWishlist(state, request) {
        state.addToWishlistDetail = request
    },
    userActiveCart(state, request) {
        state.userActiveCart = request
    },
    removedStatus(state, request) {
        state.removedStatus = request
    },
    increasedStatus(state, request) {
        state.increasedStatus = request
    },
    reducedStatus(state, request) {
        state.reducedStatus = request
    },
    movedStatus(state, request) {
        state.movedStatus = request
    },
    orderCreate(state, request) {
        state.orderDetail = request
    },
    checkoutDetail(state, request) {
        state.checkoutDetail = request
    },
    paymentSuccess(state, request) {
        state.paymentDetail = request
    },
    setDelivery(state, request) {
        state.deliveryDetail = request
    },
    Error(state, {errorCode, errorMessage}) {
        state.errorCode = errorCode
        state.errorMessage = errorMessage
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}