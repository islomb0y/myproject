import { profileService, AuthenticationError } from '../services/profile.services'

const state =  {
    activation: '',
    profile: '',
    updated: '',
    success:'',
    tokenKey: '',
    passwordUpdated: '',
    userCardList: '',
    userOrderList: '',
    userOpenOrderList: '',
    userCancelOrderList: '',
    statusIsDefault:'',
    orderDetailList:'',
    statusIsDelete:'',
    wishlist: '',
    removedStatus: '',
    updateDetail: '',
    errorCode: '',
    errorMessage: '',

}

const getters = {
    responseData: (state) => {
        return state.activation
    },
    profile: (state) =>{
        return state.profile
    },
    updatedStatus: (state) =>{
        return state.updated
    },
    emailSendedSuccess: (state) =>{
        return state.success
    },
    tokenKeySended: (state) =>{
        return state.tokenKey
    },
    passwordUpdated: (state) =>{
        return state.passwordUpdated
    },
    cardList: (state) =>{
        return state.userCardList
    },
    orderList: (state) =>{
        return state.userOrderList
    },
    openOrderList: (state) =>{
        return state.userOpenOrderList
    },
    cancelOrderList: (state) =>{
        return state.userCancelOrderList
    },
    statusIsDefault: (state) =>{
        return state.statusIsDefault
    },
    statusIsDelete: (state) =>{
        return state.statusIsDelete
    },
    wishlist: (state) =>{
        return state.wishlist
    },
    removedStatus: (state) =>{
        return state.removedStatus
    },
    updateDetail: (state) =>{
        return state.updateDetail
    },
    errorCode: (state) => {
        return state.errorCode
    },
    errorMessage: (state) => {
        return state.errorMessage
    },
}

const actions = {
    async sendActivationKey({ commit }, key){
        try {
            let request = await profileService.sendActivationKey(key);
            commit('activation', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getProfileData({ commit }){
        try {
            let request = await profileService.getProfile();
            commit('profile', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async updateProfile({ commit }, updatedData){
        try {
            let request = await profileService.updateProfile(updatedData);
            commit('updated', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async sendEmailForgetPassword({ commit }, email){
        try {
            let request = await profileService.sendEmailForgetPassword(email);
            commit('success', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async sendTokenKey({ commit }, key){
        try {
            let request = await profileService.sendTokenKey(key);
            commit('tokenKey', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async resetoldPassword({ commit }, payload){
        try {
            let request = await profileService.sendNewPassword(payload);
            commit('passwordUpdated', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getUserCardList({ commit }){
        try {
            let request = await profileService.getUserCardList();
            commit('userCardList', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getUserOrderList({ commit }){
        try {
            let request = await profileService.getUserOrderList();
            commit('userOrderList', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async orderDetailAction({ commit },orderID){
        try {
            let request = await profileService.orderDetail(orderID);
            commit('orderDetailMutation', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getUserOpenOrderList({ commit }){
        try {
            let request = await profileService.getUserOpenOrderList();
            commit('userOpenOrderList', request)
            
            
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getUserCancelOrderList({ commit }){
        try {
            let request = await profileService.getUserCancelOrderList();
            commit('userCancelOrderList', request)
            
            
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async setUserCardIsDefault({ commit }, address_id){
        try {
            let request = await profileService.setUserCardIsDefault(address_id);
            commit('setUserCardDefault', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async setUserCardIsDelete({ commit }, address_id){
        try {
            let request = await profileService.setUserCardIsDelete(address_id);
            commit('setUserCardDelete', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async getWishlistProducts({ commit }){
        try {
            let request = await profileService.getWishlistProducts();
            commit('getWishlist', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async removeProductInWishlist({ commit }, product_id){
        try {
            let request = await profileService.removeProductInWishlist(product_id);
            commit('removeWishlist', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
    async updateWishlistStatus({ commit }, wishlist_id){
        try {
            let request = await profileService.updateWishlistStatus(wishlist_id);
            commit('updateWishlist', request)
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('Error', {errorCode: e.errorCode, errorMessage: e.message})
            }
        }
    },
}
const mutations = {
    activation(state, request) {
        state.activation = request
    },
    profile(state, request) {
        state.profile = request
    },
    updated(state, request) {
        state.updated = request
    },
    success(state, request) {
        state.success = request
    },
    tokenKey(state, request) {
        state.tokenKey = request
    },
    passwordUpdated(state, request) {
        state.passwordUpdated = request
    },
    userCardList(state, request) {
        state.userCardList = request
    },
    userOrderList(state, request) {
        state.userOrderList = request
    },
    userOpenOrderList(state, request) {
        state.userOpenOrderList = request
    },
    orderDetailMutation(state, request) {
        state.orderDetailList = request
    },
    userCancelOrderList(state, request) {
        state.userCancelOrderList = request
    },
    setUserCardDefault(state, request) {
        state.statusIsDefault = request
    },
    setUserCardDelete(state, request) {
        state.statusIsDelete = request
    },
    getWishlist(state, request) {
        state.wishlist = request
    },
    removeWishlist(state, request) {
        state.removedStatus = request
    },
    updateWishlist(state, request) {
        state.updateDetail = request
    },
    Error(state, {errorCode, errorMessage}) {
        state.errorCode = errorCode
        state.errorMessage = errorMessage
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}