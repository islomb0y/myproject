import AppProducts from './products.vue';

import AppProductList from './product-list.vue';

const productRoutes = [
  {
    path: '/products',
    component: AppProducts,
    children: [
      {
        path: 'product-list',
        name: 'product-list',
        component: AppProductList
      },
      {
        path: '',
        redirect: { name: 'product-list' }
      }
    ]
  }
];

export default productRoutes;