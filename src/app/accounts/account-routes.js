import LoginView from 'src/app/accounts/auth/login-page.vue'
import RegisterView from 'src/app/accounts/auth/register-page.vue'
const accountRoutes = [
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta: {
        public: true,
        onlyWhenLoggedOut: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView,
      meta: {
        public: true,
        onlyWhenLoggedOut: true
      }
    },
  ]
  
  export default accountRoutes