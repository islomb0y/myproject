import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './main-routes'
import { TokenService } from './../services/storage.service'
// import mainRoutes from './main-routes'
// import accountRoutes from '../app/accounts/account-routes'

Vue.use(VueRouter)
  // Vue.component('loading', { template: '<div>Loading!</div>' })
  /*
   * If not building with SSR mode, you can
   * directly export the Router instantiation
   */

let Router;

export default function( /* { store, ssrContext } */ ) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    // routes: [...mainRoutes, ...accountRoutes],
    // data: { loading: false },
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    const isPublic = to.matched.some(record => record.meta.public)
    const onlyWhenLoggedOut = to.matched.some(record => record.meta.onlyWhenLoggedOut)
      // const loggedIn = localStorage.getItem('access_token') || null;
    const loggedIn = !!TokenService.getToken();

    // if (!isPublic && !loggedIn) {
    //   return next({
    //     path: '/',
    //     replace: true,
    //     query: { redirect: to.fullPath } // Store the full path to redirect the user to after login
    //   })
    // }

    // Do not allow user to visit login page or register page if they are logged in
    if (loggedIn && onlyWhenLoggedOut) {
      return next('/')
    }

    next();

  })

  return Router
}
// router.beforeEach((to, from, next) => {
//   app.loading = true
//   next()
// })
// router.afterEach((to, from, next) => {
//   setTimeout(() => app.loading = false, 1500) // timeout for demo purposes
//   next()
// })

export { Router }