import enUS from './en-us'
import uzLat from './uz-lat'
import ruRus from './ru-rus'

export default {
  'en-us': enUS,
  'uz-lat': uzLat,
  'ru-rus': ruRus
}
