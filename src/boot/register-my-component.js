// file: /src/boot/register-my-component.js
import Vue from 'vue'
import GoBtn from '../app/shared/global/go-btn.vue'
import GoCard from '../app/shared/global/go-card.vue'
import GoDrawer from '../app/shared/global/go-drawer.vue'
import GoFlatBtn from '../app/shared/global/go-btn-flat.vue'
import GoLoader from '../app/shared/global/go-loader.vue'
import GoReadmore from '../app/shared/global/go-readmore.vue'
import GoRoute from '../app/shared/global/go-route-link.vue'
import GoToggle from '../app/shared/global/go-toggle.vue'

// We globally register our component with Vue
// Note the words in '' below are component names.
// Use that names not the ones on the right.
Vue.component('go-btn', GoBtn)
Vue.component('go-card', GoCard)
Vue.component('go-drawer', GoDrawer)
Vue.component('go-flat-btn', GoFlatBtn)
Vue.component('go-loader', GoLoader)
Vue.component('go-readmore', GoReadmore)
Vue.component('go-route', GoRoute)
Vue.component('go-toggle', GoToggle)


// import something here

// "async" is optional
// export default async ({ /* app, router, Vue, ... */ }) => {
// something to do
// }
