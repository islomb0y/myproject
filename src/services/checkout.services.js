import ApiService from './api.service'


class AuthenticationError extends Error {
    constructor(errorCode, message) {
        super(message)
        this.name = this.constructor.name
        this.message = message
        this.errorCode = errorCode
    }
}

const checkoutService = {
    /**
     * Login the user and store the access token to TokenService. 
     * 
     * @returns access_token
     * @throws AuthenticationError 
     * 
    **/

    addProductToCart: async function (payload) {
        const requestData = {
            method: 'post',
            url: "/api/products/v1/add/to/cart",
            data: payload,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    addProductToWishlist: async function (payload) {
        const requestData = {
            method: 'post',
            url: "/api/products/v1/add/wishlist",
            data: payload,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getUserActiveCart: async function () {
        const requestData = {
            method: 'get',
            url: "/api/carts/v1/cart/active",
            params: {
                token: localStorage.getItem('user') && localStorage.getItem('user')
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    removeProductFromCart: async function (product_id) {
        const requestData = {
            method: 'post',
            url: "/api/carts/v1/cart/product/remove",
            data: {
                product_id: product_id
            },
            params: {
                token: localStorage.getItem('user') && localStorage.getItem('user')
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    increaseQuantity: async function (payload) {
        const requestData = {
            method: 'post',
            url: "/api/carts/v1/cart/product/update",
            data: {
                products: [
                    {
                        quantity: payload.quantity,
                        product: payload.product
                    }
                ]
            },
            params: {
                token: localStorage.getItem('user') && localStorage.getItem('user')
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    reduceQuantity: async function (payload) {
        const requestData = {
            method: 'post',
            url: "/api/carts/v1/cart/product/update",
            data: {
                products: [
                    {
                        quantity: payload.quantity,
                        product: payload.product
                    }
                ]

            },
            params: {
                token: localStorage.getItem('user') && localStorage.getItem('user')
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    cartProductMoveToWishlist: async function (product_id) {
        const requestData = {
            method: 'post',
            url: "/api/carts/v1/cart/product/remove/add/wishlist",
            data: {
                product: product_id
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    userCreateOrder: async function (address_id) {
        const requestData = {
            method: 'post',
            url: "/api/orders/v1/create",
            data: {
                shipping_address: address_id,
                billing_address: address_id
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getCheckoutDetail: async function () {
        const requestData = {
            method: 'get',
            url: "/api/billing/v1/checkout",
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    makePayments: async function (payload) {
        const requestData = {
            method: 'post',
            url: "/api/billing/v1/receipt/pay",
            data: {
                token: payload.token,
                order_id: payload.order_id
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    setDeliveryMethod: async function (payload) {
        const requestData = {
            method: 'post',
            url: "/api/orders/v1/delivery",
            data: {
                is_fragile: payload
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },






}

export default checkoutService

export { checkoutService, AuthenticationError }