import ApiService from './api.service'


class AuthenticationError extends Error {
    constructor(errorCode, message) {
        super(message)
        this.name = this.constructor.name
        this.message = message
        this.errorCode = errorCode
    }
}

const profileService = {
    /**
     * Login the user and store the access token to TokenService.
     *
     * @returns access_token
     * @throws AuthenticationError
     *
    **/


    sendActivationKey: async function(key){
        const requestData = {
            method: 'post',
            url: "/api/auth/v1/activated",
            data: {key: key}
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getProfile: async function(){
        const requestData = {
            method: 'get',
            url: "/api/profiles/v1/update"
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    updateProfile: async function(updatedData){
        const requestData = {
            method: 'put',
            url: "/api/profiles/v1/update",
            data: updatedData
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    sendEmailForgetPassword: async function(email){
        const requestData = {
            method: 'post',
            url: "/api/auth/v1/send/email",
            data: {email: email}
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data.success
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    sendTokenKey: async function(key){
        const requestData = {
            method: 'post',
            url: "/api/auth/v1/token/control",
            data: {key: key}
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data.success
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    sendNewPassword: async function(payload){
        const requestData = {
            method: 'post',
            url: "/api/auth/v1/password/reset",
            data: {
                key: payload.key,
                password: payload.password
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data.update
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getUserCardList: async function(){
        const requestData = {
            method: 'get',
            url: "/api/billing/v1/user/cards"
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getUserOrderList: async function(){


        const requestData = {
            method: 'get',
            url: "/api/orders/v1/list"
        }
        try {
            const response = await ApiService.customRequest(requestData)

            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    orderDetail: async function(orderID){
        const requestData = {
          method: 'get',
          url: `/api/orders/v1/new/detail/${orderID}`
        }
        try {
          const response = await ApiService.customRequest(requestData)
          return response
        } catch (e) {
         console.log(e)
        }
      },
    getUserOpenOrderList: async function(){
        const requestData = {
            method: 'get',
            url: "/api/orders/v1/open/order"
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getUserCancelOrderList: async function(){
        const requestData = {
            method: 'get',
            url: "/api/orders/v1/cancel"
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    // getUserOpenOrderList: async function(){
    //     const requestData = {
    //         method: 'get',
    //         url: "/api/orders/v1/open/order"
    //     }
    //     try {
    //         const response = await ApiService.customRequest(requestData)
    //         return response.data
    //     } catch (error) {
    //         throw new AuthenticationError(error.response.status, error.response.data.detail)
    //     }
    // },
    setUserCardIsDefault: async function(address_id){
        const requestData = {
            method: 'put',
            url: "/api/billing/v1/set_default_card/"+address_id,
            data: {
                is_default:true
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    setUserCardIsDelete: async function(address_id){
        const requestData = {
            method: 'put',
            url: "/api/billing/v1/card_delete/"+address_id,
            data: {
                is_deleted:true
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getWishlistProducts: async function(){
        const requestData = {
            method: 'get',
            url: "/api/products/v1/wishlist/product/list",
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    removeProductInWishlist: async function(product_id){
        const requestData = {
            method: 'delete',
            url: "/api/products/v1/wishlist/product/remove/"+product_id,
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    updateWishlistStatus: async function(wishlist_id){
        const requestData = {
            method: 'put',
            url: "/api/products/v1/wishlist/update",
            data: {
                id: wishlist_id
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },






}

export default profileService

export { profileService, AuthenticationError }
