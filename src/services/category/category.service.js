import ApiService from "../api.service";



const CategoryService = {
 compare( a, b ) {
  if ( a.name < b.name ){
    return -1;
  }
  if ( a.name > b.name ){
    return 1;
  }
  return 0;
},

  async rootCategory() {
    const requestData = {
      method: 'get',
      url: '/api/category/v1/list/'
    }
    try {
      let response = await ApiService.customRequest(requestData)
      response.data.Cat.sort(this.compare)
      return response
    } catch (e) {
      console.log('Error in getting root Category')
    }

  },
  async trendingProducts(){

      const requestData = {
        method: 'get',
        url: `/api/products/v1/merchandising`    }

    try {
      let response = await ApiService.customRequest(requestData)
      return response
    } catch(e)  {
      console.log('Error in trending prodycts Category')
    }

  },

  async trendingProductsWithVatId(catId){
    const requestData = {
      method: 'get',
      url: `/api/products/v1/merchandising?catid=${catId}`    }

  try {
    let response = await ApiService.customRequest(requestData)
    return response
  } catch(e)  {
    console.log('Error in trending prodycts Category')
  }
  },

  async categoryList(catID){
    const requestData = {
      method: 'get',
      url: `api/category/v1/sub/?cat-id=${catID}`
    }
    try {
      let response = await ApiService.customRequest(requestData)

      return response
    } catch (e) {
      console.log('Error in getting root Category')
    }

  },

  async updateCategoryLang(langValue) {
    const requestData = {
      method: 'get',
      url: `api/profiles/v1/update/language/?language=${langValue}`
    }
    try {

      let response = await ApiService.customRequest(requestData)

      return response
    } catch (e) {
      console.log('Error in getting root Category')
    }

  },

  async fetcProductList(payload) {
    const requestData = {
      method: 'get',
      url: `api/products/v1/multiple/filters?productsPerPage=20&pagenum=${payload.cpn}&catid=${payload.cid}`
    }
    try {
      let response = await ApiService.customRequest(requestData)
      return response
    } catch (e) {
      throw e
    }
  }
}

export default CategoryService
